#!/usr/bin/env bash
# Counter Strike launcher

cd ~/.wine/drive_c/Games/Counter-Strike

wine start CS16Launcher.exe -steam -game cstrike -noforcemparms -noforcemaccel
