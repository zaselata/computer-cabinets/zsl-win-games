zsl-win-games
=========

Инсталира игри и приложения от трети източници под Wine

Requirements
------------

N/a

Role Variables
--------------

* zsl_win_games_cs_install : [true]
* zsl_win_games_plantsvszombies_install: [true]
* zsl_win_games_starcraft_install: [true]

* zsl_win_games_wineprefix_url: [http://10.200.11.2:/games/wine-games.tar.gz]
* zsl_win_games_cs_url: [http://10.200.11.2:/games/cs.tar.gz]
* zsl_win_games_starcraft_url: [http://10.200.11.2:/games/starcraft.tar.gz]
* zsl_win_games_plantsvszombies_url: [http://10.200.11.2:/games/plantsvszombies.tar.gz]

Dependencies
------------

N/a

Примерен Playbook
----------------

    - hosts: localhost
      roles:
         - { role: zsl_win_games,
                   zsl_win_games_starcraft_install: No }

License
-------

GPLv3

Author Information
------------------

 - Blagovest Petrov
